<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php
//Arreglo dónde se tendrá la información del archivo CSV
    $vuelos = array(
        array('id','Origen','Destino','Duración'),
        array('1','New York','London','415'),
        array('2','Shangai','Paris','760'),
        array('3','Istambul','Tokyo','700'),
        array('4','New York','Paris','415'),
        array('5','Moscow','Paris','245'),
        array('6','Lima','New York','455'),
    );

    //nombre del archivo que se creará
    $nombreArch = 'vuelos2.csv';

    //Aquí se abre el archivo de modo de escritura
    $archivo = fopen($nombreArch, 'w') or die ("No se puede abrir el archivo: $nombreArch");

    //Leyendo todos los arreglos
    foreach($vuelos as $vuelos){
        // se reescribe el archivo en cada registro
        $resp = fputcsv($archivo, $vuelo, ',', '"', '"');

        //si el resultado de intentar escribir es estrictamente igual a false
        if ($resp === false) {die ("Error al escribir en CSV"); }
    }
    //fin del foreach
    //Cerramos archivo
    fclose($archivo) or die("No se puede cerrar el archivo: $nombreArch");
    echo ("<h1> Revisa el archivo: $nombreArch </h1>");
?>
</body>
</html>